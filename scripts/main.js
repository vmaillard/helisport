
document.addEventListener("DOMContentLoaded", function() {
	init();
}, false);


function init() {

	var opener = document.getElementById('opener');
	opener.addEventListener("click", close_opener);

	check_audio();
	ajax_interactions();
	polyfill_object_fit_images();
	// adapt_svg();

	// ajout d'une class aux li qui contiennent des liens
	specific_link_li();

	// suppression margin bottom du dernier enfant
	last_child_margin();

	var a_nav = document.querySelectorAll('h2 a.ajax-link');
	a_nav.forEach(function (item) {
		item.addEventListener('mouseover', add_paused, false);
		item.addEventListener('mouseleave', remove_paused, false);
	});


	var a_tags = document.querySelectorAll('a[data-href]:not(.ajax-link)');
	a_tags.forEach(function (item) {
		item.addEventListener('click', change_location, false);
	});

	// mise en place des srcset pour vidéos
	// videoSourceSet();

	set_slider();

	// polyfill sticky
	var stickies = document.querySelectorAll('.sticky');
	Stickyfill.add(stickies);

	var audio_button = document.getElementById('play-pause-button');
	var audio = document.getElementById('audio');
	audio_button.addEventListener("click", play_pause_audio);
	audio_button.addEventListener("mouseenter", mouseenter_audio);
	audio_button.addEventListener("mouseleave", mouseleave_audio);
	audio.onended = function() {
		audio_button.classList.add("play");
		audio_button.classList.remove("pause");
	};

	// scroll listener
	// set_highlight_scroll();
	highlight_scroll();
	registerListener('scroll', highlight_scroll);
	registerListener('resize', highlight_scroll);

	
	if ($('.button_scroll').length) {
		var button_scroll = document.getElementsByClassName("button_scroll")[0];
		button_scroll.addEventListener('click', scroll_to);
	}
	

	// interactions tableau
	var tableau_titres = document.querySelectorAll(".tableau_titre");
	tableau_titres.forEach(function (item) {
		item.addEventListener("click", show_tableau);
	});

	// var isMobile = window.matchMedia("only screen and (max-width: 768px) and (max-device-width : 768px)");
	// var isMobileLandscape = window.matchMedia("only screen and (max-device-width : 1030px) and (orientation : landscape)");
	// if (isMobile.matches == true || isMobileLandscape.matches == true) {
	// interaction burger
	var picto_burger = document.getElementsByClassName("picto_burger")[0];
	picto_burger.addEventListener("click", toggle_burger);
	// }
	
}

function close_opener() {
	// this.classList.add("disabled");
	$(this).fadeOut(230);
	var body = document.getElementsByTagName('body')[0];
	body.classList.remove("noscroll");
}
function adapt_svg() {
    // pour adpater les svg 
    var w = window,
    d = document,
    e = d.documentElement,
    g = d.getElementsByTagName('body')[0],
	y = w.innerWidth|| e.clientWidth|| g.clientWidth;
	var isMobile = window.matchMedia("only screen and (max-width: 768px) and (max-device-width : 768px)");
    var isMobileLandscape = window.matchMedia("only screen and (max-device-width : 1030px) and (orientation : landscape)");
    
    if (isMobile) {
        var svg_burger = document.getElementById('burger');
        var svg_cross = document.getElementById('cross');
        var svg_play = document.getElementById('play');
        var svg_pause = document.getElementById('pause');
        
        svg_burger.setAttribute("viewBox", "2.5 0 62 45.5");
        svg_cross.setAttribute("viewBox", "2.5 0 62 45.5");
        svg_play.setAttribute("viewBox", "0 -1 28 26");
        svg_pause.setAttribute("viewBox", "0 -1 28 26");
    } else if (isMobileLandscape.matches && y <= 768) {
        var svg_burger = document.getElementById('burger');
        var svg_cross = document.getElementById('cross');
        var svg_play = document.getElementById('play');
        var svg_pause = document.getElementById('pause');
        
        svg_burger.setAttribute("viewBox", "2.5 0 62 45.5");
        svg_cross.setAttribute("viewBox", "2.5 0 62 45.5");
        svg_play.setAttribute("viewBox", "0 -1 28 26");
        svg_pause.setAttribute("viewBox", "0 -1 28 26");
    } else {
        var svg_burger = document.getElementById('burger');
        var svg_cross = document.getElementById('cross');
        var svg_play = document.getElementById('play');
        var svg_pause = document.getElementById('pause');

        svg_play.setAttribute("viewBox", "-1 -7 86.8 79.36");
        svg_pause.setAttribute("viewBox", "-1 -7 86.8 79.36");
    }
}

// ajout .paused pour les h2 de la nav
function add_paused() {
	this.parentElement.parentElement.parentElement.classList.add("paused");
}
function remove_paused() {
	this.parentElement.parentElement.parentElement.classList.remove("paused");
}


// suppression margin bottom du dernier enfant
function last_child_margin() {
	var article = document.querySelectorAll(".module.right article");
	article.forEach(function (item, index) {
		if (item.lastElementChild.classList.contains("gallery")) {
			item.lastElementChild.classList.add("margin_debug");
			
			if (index === article.length - 1){ 
				console.log(item);
				item.classList.add("overflow_debug");
			}
			/*
			if(last article) {
				article add overflow_debug
			}
			*/
		}
	});
	
}


// pour les li qui contiennent des liens
function specific_link_li() {
	var li = document.querySelectorAll("ul:not(.ligne):not(.container_tableau):not(.credits) li");
	li.forEach(function (item) {
		item.innerHTML = "— " + item.innerHTML;
		if(item.querySelector("a") != null) {
			item.classList.add("link");
		}
	});
}

function change_location() {
	var url = this.getAttribute('data-href');
	window.location.href = url;
	// var target = this.getAttribute('target');
	// if (target == null) target = "_self";
}


// sur la home
function scroll_to() {
	var scroll_anchor = $('.scroll_anchor:eq(0)');
	// console.log(scroll_anchor);
	var top = 0;
	if ($('#site_intro').length) top = 28;
	$('html, body').animate({
		scrollTop: scroll_anchor.position().top + top
	}, 1000, 'easeInOutExpo');
}

/*
$("#logo").click(function(){
	$("html, body").animate({
		scrollTop: $('#home').offset().top-42
	}, 700, 'easeOutCubic');
});
*/

// sur mobile
function toggle_burger() {
	var picto_burger = document.getElementsByClassName("picto_burger")[0];
	picto_burger.classList.toggle("open");
	var header = document.getElementsByTagName("header")[0];
	header.classList.toggle("open");
}

function show_tableau() {
	$(this).next().slideToggle({ duration: 600, easing: "easeOutCubic" });
}

// audio 

function check_audio() {
	var body = document.getElementsByTagName('body')[0];
	var audio = document.getElementById('audio');
	var audio_button = document.getElementById('play-pause-button');
	var promise = audio.play();
	//var h1 = 
	if (promise !== undefined) {
	promise.then(_ => {
		// autoplay
	}).catch(error => {
		// not autoplay
		
		audio_button.classList.add("play");
		audio_button.classList.remove("pause");
		body.addEventListener("click", debug_play_pause_audio);
	});
	}
}
function play_pause_audio() {
	
	var audio = document.getElementById('audio');
	var body = document.getElementsByTagName('body')[0];
	var audio_button = document.getElementById('play-pause-button');

	var id_play = document.getElementById("play");
	var id_pause = document.getElementById("pause");
	var isMobile = window.matchMedia("only screen and (max-width: 768px) and (max-device-width : 768px)");
    var isMobileLandscape = window.matchMedia("only screen and (max-device-width : 1030px) and (orientation : landscape)");
    
	if (audio_button.classList.contains("play")) {
		audio_button.classList.remove("play");
		audio_button.classList.add("pause");
		audio.play();
		if (isMobile.matches != true && isMobileLandscape.matches != true) {
		id_pause.classList.add("hover");
		}
	} else {
		audio_button.classList.add("play");
		audio_button.classList.remove("pause");
		audio.pause();

		if (isMobile.matches != true && isMobileLandscape.matches != true) {
		id_play.classList.add("hover");
		}
	}

}

function debug_play_pause_audio() {
	var audio = document.getElementById('audio');
	var body = document.getElementsByTagName('body')[0];
	var audio_button = document.getElementById('play-pause-button');

	if (audio_button.classList.contains("play")) {
		audio_button.classList.remove("play");
		audio_button.classList.add("pause");
		audio.play();
		body.removeEventListener("click", debug_play_pause_audio);
	} else {
		audio_button.classList.add("play");
		audio_button.classList.remove("pause");
		audio.pause();
	}
}

function mouseenter_audio() {
	var isMobile = window.matchMedia("only screen and (max-width: 768px) and (max-device-width : 768px)");
    var isMobileLandscape = window.matchMedia("only screen and (max-device-width : 1030px) and (orientation : landscape)");
    
	var id_play = document.getElementById("play");
	var id_pause = document.getElementById("pause");
	if (this.classList.contains("play")) {
		if (isMobile.matches != true && isMobileLandscape.matches != true) {
		id_play.classList.add("hover");
		}
	} else {
		if (isMobile.matches != true && isMobileLandscape.matches != true) {
		id_pause.classList.add("hover");
		}
	}
}
function mouseleave_audio() {
	var id_play = document.getElementById("play");
	var id_pause = document.getElementById("pause");
	id_pause.classList.remove("preview","hover");
	id_play.classList.remove("preview","hover");
}




function set_slider() {
	
	$('.gallery').slickLightbox({
		itemSelector: '> figure > a',
		// background: 'rgba(0,0,0,0)',
		// imageMaxHeight: 0.85,
		caption: 'caption',
		lazy: true,
		src: 'src',
		destroyTimeout: 0,
		srcset: 'srcset',
		layouts: {
			closeButton: '',
		},
		slick: {
			slidesToShow: 1,
			speed: 0,
			lazyLoad: 'progressive',
			fade: true,
			nextArrow: '.slick-next',
			prevArrow: '.slick-prev'
		  }
	});



}


function slide_events() {

	// lier les events slider aux boutons 
	var slick_prev = document.querySelector(".slick-prev");
	slick_prev.addEventListener("click", go_to_prev_slide);
	var slick_next = document.querySelector(".slick-next");
	slick_next.addEventListener("click", go_to_next_slide);

}

function go_to_prev_slide() {
	$('.gallery').slick('slickPrev');
}

function go_to_next_slide() {
	$('.gallery').slick('slickNext');
}

function polyfill_object_fit_images() {
	var someImages = document.querySelectorAll('.container_grid figure img');
	objectFitImages(someImages);
}

function ajax_interactions() {
	
	var href;
	var title;

  	$('body').on('click','a.ajax-link',function(e) { // nav link clicked

    	// check to see it contents already accessed
    	if($(this).parent().parent().hasClass('current')){

        	console.log('current page');

			e.preventDefault();
			// arrêt fonction
        	return true;
    	}

      	$('.current').removeClass('current'); // remove .current from active link
		$('.parent').removeClass('parent'); // remove .current from active link
		$(this).parent().parent().parent().parent().addClass('current'); // add .current
		
		// click sur une vidéo de la home, ajout de .current au div>h2>nav correspondant
		if ($(this).parent('div').length) {
			var name_attr = $(this).attr("name");
			$('#page_links a[name='+name_attr+']').parent().parent().addClass('current');
		}

		var parent = $(this).attr('parent');
		if(parent){ // if link page has parent nav
			$('nav a[name="'+parent+'"]').addClass('parent'); // add .current
		}

		// add progress spinner on cursor
		$('body').css('cursor','progress');

		// remove cusor spinner
		var pointerTimeout = setTimeout(function () {
			$('body').css('cursor','auto'); // progress spinner on cursor
		}, 1000);

		href = $(this).attr("data-href");
      	title = $(this).attr("name"); // on attrape l'attribut "name" pour l'onglet

		// load content via AJAX
      	loadContent(href);

		// add new url to html5 history
      	history.pushState('', +href, href); // push url to history

		// change le titre (<head>) de l'onglet
      	$('title').html(title);

		// prevent click and reload
		e.preventDefault();
		
		 
	});

    // for back / forwards browser navigation
    var toggleHistoryVisitNum = 0;
    function toggleHistory(){
        window.onpopstate = function(event) {
            if(window.location.hash == ""){
            	$('.current').removeClass('current');
                loadContent(location.pathname);
            }
        };
    }

	function loadContent(url) { // Load content

		// variable for page data
    	$pageData = '';

		// send Ajax request
    	$.ajax({
	        type: "POST",
	        url: url,
	        data: { ajax: true },
	        success: function(data,status) {
				$pageData = data;
      		}
    	}).done(function(){

			// construct new content
      		$pageData = '<div class="content no-opacity ajax">' + $pageData + '</div>';

			// add content to page
			$('main').append($pageData);

			// remove old content
		    $('.content.current-content').remove();


			// show new content and clean up classes
		    $('.content.ajax').removeClass('no-opacity').removeClass('ajax').addClass('current-content');
			
	      	// toggle use of back and forward buttons on browser
	      	if(!toggleHistoryVisitNum) {
	        	toggleHistory();
	      	}

			// keep track of number of Ajax requests
			toggleHistoryVisitNum++;


			//retour en haut
			$("html,body").scrollTop(0);
			
			// appel d'autres fonctions js
			set_slider();
			set_highlight_scroll();
			highlight_scroll();
			// mise en place des srcset pour vidéos
			// videoSourceSet();
			

			var tableau_titres = document.querySelectorAll(".tableau_titre");
			tableau_titres.forEach(function (item) {
				item.addEventListener("click", show_tableau);
			});

			// pour les data-href
			var a_tags = document.querySelectorAll('a[data-href]:not(.ajax-link)');
			a_tags.forEach(function (item) {
				item.addEventListener('click', change_location, false);
			});

			// sur home
			if ($('.button_scroll').length) {
				var button_scroll = document.getElementsByClassName("button_scroll")[0];
				button_scroll.addEventListener('click', scroll_to);
			}

			var header = document.getElementsByTagName("header")[0];
			// si header ouvert, on le ferme
			if (header.classList.contains("open")) {
			toggle_burger();
			}
			
			// pour les li qui contiennent des liens
			specific_link_li();

			// suppression margin bottom du dernier enfant
			last_child_margin();

    	}); // end of ajax().done()
  	} // end of loadContent()
} // fin ajax_interactions();
