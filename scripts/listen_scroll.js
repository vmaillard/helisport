
var video = [];
function set_highlight_scroll(){
	video = document.querySelectorAll("video");
    console.log('Found ' + video.length + ' video images');
} 

function play_vid(el) {
	el.play();
}

function pause_vid(el) {
	el.pause();
}


function highlight_scroll(){
    // pour adpater les svg 
    adapt_svg();
    // pour adpater les svg 
    var w = window,
    d = document,
    e = d.documentElement,
    g = d.getElementsByTagName('body')[0],
    y = w.innerWidth|| e.clientWidth|| g.clientWidth;
    var isMobileLandscape = window.matchMedia("only screen and (max-device-width : 1030px) and (orientation : landscape)");
    
    if (y <= 480) {
        var svg_burger = document.getElementById('burger');
        var svg_cross = document.getElementById('cross');
        var svg_play = document.getElementById('play');
        var svg_pause = document.getElementById('pause');
        
        svg_burger.setAttribute("viewBox", "2.5 0 62 45.5");
        svg_cross.setAttribute("viewBox", "2.5 0 62 45.5");
        svg_play.setAttribute("viewBox", "0 -1 28 26");
        svg_pause.setAttribute("viewBox", "0 -1 28 26");
    } else if (isMobileLandscape.matches && y <= 750) {
        var svg_burger = document.getElementById('burger');
        var svg_cross = document.getElementById('cross');
        var svg_play = document.getElementById('play');
        var svg_pause = document.getElementById('pause');
        
        svg_burger.setAttribute("viewBox", "2.5 0 62 45.5");
        svg_cross.setAttribute("viewBox", "2.5 0 62 45.5");
        svg_play.setAttribute("viewBox", "0 -1 28 26");
        svg_pause.setAttribute("viewBox", "0 -1 28 26");
    } else {
        var svg_burger = document.getElementById('burger');
        var svg_cross = document.getElementById('cross');
        var svg_play = document.getElementById('play');
        var svg_pause = document.getElementById('pause');

        svg_play.setAttribute("viewBox", "-1 -7 86.8 79.36");
        svg_pause.setAttribute("viewBox", "-1 -7 86.8 79.36");
    }

	var isMobile = window.matchMedia("only screen and (max-width: 768px) and (max-device-width : 768px)");
	var isMobileLandscape = window.matchMedia("only screen and (max-device-width : 1030px) and (orientation : landscape)");
    
    // si sur desktop
	if (isMobile.matches == false && isMobileLandscape.matches == false && y >= 768) {
        
        // si on est sur la home
        // on fade la #site_intro % scroll top
        if ($('#site_intro').length) {

        var scroll_top  = window.pageYOffset || document.documentElement.scrollTop;

        /*
        var e = 100;
        var t = 200;
        var i = 0;
        if (scroll_top <= e) {
            i = 1;
        } else if (scroll_top <= t) {
            i = 1 - scroll_top / t;
        }
        $('#site_intro').css("opacity", i);
        */
        
        
        if (scroll_top > 100) {
            $('#site_intro').fadeOut(0);
        } else {
            $('#site_intro').fadeIn(0);
        }
        

        /*
        if (scroll_top > 100) {
            var height = $('#site_intro').height();
            console.log(height);
            $('#site_intro').css( "position", "absolute" );
            $('#site_intro').css( "top", height * (-1) );
            
        } else {
            // $('#site_intro').slideDown(0);
            $('#site_intro').css( "top", "170px");
            $('#site_intro').css( "position", "sticky" );
        }
        */
        

        }
    } else {
        $('#site_intro').css("opacity", 1);
    }
    /*
    if($('.scroll_leading').length) {
    
    var top  = window.pageYOffset || document.documentElement.scrollTop;
    var top_el = $('.scroll_leading')[0].getBoundingClientRect().top;
    var offset_top_el = $('.scroll_leading').position().top;
    var interval = 50;
    var hauteur_header = 170;

    if (top_el > hauteur_header - interval && top_el < hauteur_header + interval) {
        
        if (complete) {
            complete = false;
        } else {
            $("html,body").animate({
                scrollTop: offset_top_el
            }, 500, function() {
                complete = true;
            });
        }

    }
    */

    /*
    var lastScrollTop = 0, delta = 5;
    var st = $(this).scrollTop();
       
    if(Math.abs(lastScrollTop - st) <= delta)
        return;
       
    if (st > lastScrollTop){
        // downscroll code
        console.log('scroll down');
    } else {
        // upscroll code
        console.log('scroll up');
    }
    lastScrollTop = st;
    */
    
    /*
    }
    */


    for(var i=0; i<video.length; i++){
        if(isInViewportMiddle(video[i])){
            video[i].classList.add("playing");
            // sur mobile, on ajoute au <a> .playing pr display le .black_filter et . video_title
            if (isMobile.matches == true || isMobileLandscape.matches == true) {
                if (video[i].parentElement.classList.contains("ajax-link")) {
                    video[i].parentElement.classList.add("playing");
                }
            }
        } else {
            if (video[i].classList.contains("playing")) {
            video[i].classList.remove("playing");
            // sur mobile
            if (isMobile.matches == true || isMobileLandscape.matches == true) {
                video[i].parentElement.classList.remove("playing");
            }
            }
        }
        
        if(isInViewportInterval(video[i])){
          	// console.log(video[i]);
            play_vid(video[i]);
        } else {                
            pause_vid(video[i]);
		}
    }
    // cleanvideo();
}



function cleanvideo(){
    video = Array.prototype.filter.call(video, function(l){ return l.getAttribute('data-scrolled');});
}

// interval
function isInViewportInterval(el){
    var rect = el.getBoundingClientRect();
    var isMobile = window.matchMedia("only screen and (max-width: 768px) and (max-device-width : 768px)");
	var isMobileLandscape = window.matchMedia("only screen and (max-device-width : 1030px) and (orientation : landscape)");
    if (isMobile.matches == true || isMobileLandscape.matches == true) {
        var bottom_header = 66;
    } else {
        var bottom_header = 170;
    }

    
    return (
        rect.bottom >= bottom_header && 
        rect.right >= 0 && 
        rect.top <= ((window.innerHeight) || document.documentElement.clientHeight) && 
        rect.left <= (window.innerWidth || document.documentElement.clientWidth)
     );
}

function isInViewportMiddle(el){
	var rect = el.getBoundingClientRect();

    return (
        rect.bottom >= (((window.innerHeight - 10) /2) || document.documentElement.clientHeight) && 
        rect.right >= 0 && 
        rect.top <= ((window.innerHeight /2) || document.documentElement.clientHeight) && 
        rect.left <= (window.innerWidth || document.documentElement.clientWidth)
     );
}



// top
function isInViewportTop(el){
    var rect = el.getBoundingClientRect();
    
    return (
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && 
        rect.right >= 0 && 
        rect.top <= 0 && 
        rect.left <= (window.innerWidth || document.documentElement.clientWidth)
     );
}

// bottom
function isInViewportBottom(el){
    var rect = el.getBoundingClientRect();
    
    return (
        rect.bottom >= 0 && 
        rect.right >= 0 && 
        rect.top <= (window.innerHeight || document.documentElement.clientHeight) && 
        rect.left <= (window.innerWidth || document.documentElement.clientWidth)
     );
}


function registerListener(event, func) {
    if (window.addEventListener) {
        window.addEventListener(event, throttle(func, 70));
    } else {
        window.attachEvent('on' + event, func)
    }
}


// fonction pour alléger l'event scroll et resize
var throttle = function(func, wait) {

    var context, args, timeout, throttling, more, result;
    return function() {
      context = this; args = arguments;
      var later = function() {
        timeout = null;
        if (more) func.apply(context, args);
      };
      if (!timeout) timeout = setTimeout(later, wait);
      if (throttling) {
        more = true;
      } else {
        result = func.apply(context, args);
      }
      throttling = true;
      return result;
    };
};

