<?php


/**
* Builds a nested list (menu items) of a single menu.
* 
* A recursive function to display nested list of menu items.
*
* @access private
* @param Int $parent ID of menu item.
* @param Array $menu Object of menu items to display.
* @param Int $first Helper variable to designate first menu item.
* @return string $out.
*
*/
function buildMenuFromObject($parent = 0, $menu, $first = 0) {
  if(!is_object($menu)) return;
  $out = '';
  $has_child = false;
  $count = 1;
  foreach ($menu as $m) {
    $newtab = $m->newtab ? " target='_blank'" : '';            
    // if this menu item is a parent; create the sub-items/child-menu-items
    if ($m->parentID == $parent) {// if this menu item is a parent; create the inner-items/child-menu-items
        // if this is the first child
        if ($has_child === false) {                    
            $has_child = true;// This is a parent                        
            if ($first == 0){                            
              $out .= "<nav id='page_links'>\n";                            
              $first = 1;
            }                        
            else $out .= "\n<ul class='sub-menu'>\n";
        }
        $class = $m->isCurrent ? ' current' : '';
		// a menu item
		$out .= "<div class='container_".$count . $class . "'>\n<div class='Orbit'>\n<div class='Electron'>\n<h2 class='anim_".$count."'>\n";
        $out .= '<a name="'.$m->title.'" class="ajax-link" data-href="' . $m->url . '"' . $newtab . '>' . $m->title;                    
        // if menu item has children
        if ($m->isParent) {
          $out .= '<span class="drop-icon">▼</span>' .
              '<label title="Toggle Drop-down" class="drop-icon" for="' . wire('sanitizer')->pageName($m->title) . '" onclick>▼</label>' .
          '</a>' .
          '<input type="checkbox" id="' . wire('sanitizer')->pageName($m->title) . '">';
        }
        
        else $out .= "</a>\n";         
        // call function again to generate nested list for sub-menu items belonging to this menu item. 
        $out .= buildMenuFromObject($m->id, $menu, $first);
        $out .= "</h2>\n</div>\n</div>\n</div>\n";
    }// end if parent
	$count++;
  }// end foreach
  if ($has_child === true) $out .= "</nav>\n";    
  return $out;
}