<?php

// /site/config.php : $config->prependTemplateFile = '_init.php';



// ——————--- déclaration de variables pour _main.php + template lié —————— //


$homepage = $pages->get('/');
$homepage_children = $homepage->children();
$page_children = $page->children();
$images = $page->images;

if ($homepage->id != $page->id) {		// si autre page
	$title = $page->title;
} else {								// si homepage
	$title = $homepage->site_title;
}

function MakeReadable($bytes, $decimals = 2) {
    $size = array('o','ko','Mo','Go','To','Po','Eo','Zo','Yo');
    $factor = floor((strlen($bytes) - 1) / 3);
    return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$size[$factor];
}

function orthotypographie($subject) {
    $search  = array(' :', ' ?', ' !', ' ;');
    $replace = array('&#8239;:', '&#8239;?', '&#8239;!', '&#8239;;');
    $result = str_replace($search, $replace, $subject);
    return $result;
}


include_once("./_func.php");			// ajout de fonctions
