<?php $options = array('upscaling' => true); ?>
<?php 
$count = $images->count;
if ($count <= 9) {
    $count_class = "count_".$count;
} else {
    $count_class = "count_multiple";
}
?>
<div class="gallery full container_grid padding <?=$count_class?> margin_debug">

<?php foreach ($images as $image): ?>
<?php
$image150 = $image->width("150px", $options);
$image200 = $image->width("200px", $options);
$image400 = $image->width("400px", $options);
$image1 = $image->width("500px", $options);
$image3 = $image->width("1000px", $options);
$image4 = $image->width("1500px", $options);
$image5 = $image->width("2000px", $options);
// $image6 = $image->width("2500px", $options);
?>
        <figure>
            <a src="<?=$image->url?>" srcset="<?=$image->width(500)->url?>, <?=$image->width(1000)->url?>, <?=$image->width(1500)->url?>, <?=$image->width(2000)->url?>" data-caption="<?=orthotypographie($image->description);?>">
                <img class="lazyload" data-sizes="auto" data-src="<?=$image->width(400)->url?>" data-srcset="<?=$image->width(150)->url?> 150w, <?=$image->width(200)->url?> 200w, <?=$image->width(400)->url?> 400w, <?=$image->width(500)->url?> 500w">
            </a>
        </figure>
        
<?php endforeach ?>

</div> <!--/gallery-->

    <footer class="container_module">
        <div class="module left padding">
            <div class="contact">
            <?=orthotypographie($homepage->body_sup);?> 
            </div>
        </div>

        <div class="module right padding">
            <div class="credits">
            <?=orthotypographie($page->credits);?> 
            </div>
        </div>
    </footer>
