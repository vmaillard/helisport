<?php

$homepage = $pages->get('/');
$homepage_children = $homepage->children();
$helicopter_pages = $pages->find("template=helicopter-page,sort=sort");

if ($homepage->id != $page->id) {		// si autre page
	$title = $page->title;
} else {								// si homepage
	$title = $homepage->site_title;
}
?>
<!DOCTYPE html>
<html lang="fr">

<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="initial-scale=1, maximum-scale=1">

	<title><?php echo $title; ?></title>
	<?php
	include("inc/style.php");
	?>

	<link rel="stylesheet" href="<?=$config->urls->templates?>styles/main.css" />
	<link rel="stylesheet" href="<?=$config->urls->templates?>styles/slick.css" />
	<link rel="stylesheet" href="<?=$config->urls->templates?>styles/slick-lightbox.css" />
	

	<script src="<?=$config->urls->templates?>scripts/jquery-2.2.4.min.js" defer></script>
	<script src="<?=$config->urls->templates?>scripts/jquery-ui.min.js" defer></script>
	
	<script src="<?=$config->urls->templates?>scripts/ofi.min.js" defer></script>
	<script src="<?=$config->urls->templates?>scripts/stickyfill.min.js" defer></script>

	<script src="<?=$config->urls->templates?>scripts/video-srcset.js" defer></script>

	<script src="<?=$config->urls->templates?>scripts/slick.min.js" defer></script>
	<script src="<?=$config->urls->templates?>scripts/slick-lightbox.js" defer></script>

	<script src="<?=$config->urls->templates?>scripts/lazysizes.min.js" defer></script>

	<script src="<?=$config->urls->templates?>scripts/main.js" defer></script>
	<script src="<?=$config->urls->templates?>scripts/listen_scroll.js" defer></script>

	</head>
	<body class="noscroll">

<?php
$homepage = $pages->get('/');
$homepage_children = $homepage->children();
?>

<div id="opener">
		<figure>
			<img src="<?=$config->urls->assets?>files/picto/logo_helisport.svg">
		</figure>
</div>

<header class="sticky">


<?php
// mise en place du menu
$mb = $modules->get('MarkupMenuBuilder');
$menuItems = $mb->getMenuItems('Main', 2);
echo buildMenuFromObject(0, $menuItems);
?>


	<nav id="titling">
			
		<div>		

			<h1>
				<a name="<?=$homepage->title?>" class="ajax-link" data-href="<?=$homepage->url?>"><?=$homepage->title?> <span><?=$homepage->sub_title?></span></a>
			</h1>
			
			<a id="play-pause-button" class="pause">
				
				<figure id="pause">
					<img src="<?=$config->urls->assets?>files/picto/pause.svg">
					<img src="<?=$config->urls->assets?>files/picto/pause_grey.svg">
				</figure>

				<figure id="play">
					<img src="<?=$config->urls->assets?>files/picto/play.svg">
					<img src="<?=$config->urls->assets?>files/picto/play_grey.svg">
				</figure>
			
			</a>
			
			<!--<audio id="audio">>-->
			<audio id="audio" autoplay loop>
				<source src="<?=$config->urls->assets.'files/1.mp3'?>" type="audio/wav">
				Your browser does not support the audio element.
			</audio>
		</div>

		<figure class="picto_burger">

			<figure id="burger">
				<img src="<?=$config->urls->assets?>files/picto/burger.svg">
			</figure>

			<figure id="cross">
				<img src="<?=$config->urls->assets?>files/picto/cross.svg">
			</figure>
			
		</figure>
	</nav>

</header>

<main>

<div class="content current-content">
