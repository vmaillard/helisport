<?php
$helicopter_pages = $pages->find("template=helicopter-page,sort=sort");
?>
<div id="container_flex_intro">

    <div class="container_site_intro">
        <div id="site_intro" class="sticky module padding left">
            <?=orthotypographie($page->body);?> 
        </div>
        
    </div>
    


    <div class="container_vid_intro button_scroll">
    <div class="sticky container_video_100">
        <video muted loop poster="<?=$config->urls->assets?>files/videos/<?=$page->video_index;?>.jpg" preload="metadata">
            <source src="<?=$config->urls->assets?>files/videos/<?=$page->video_index;?>.m4v" type="video/mp4">
        </video>
    </div>
    </div>
    
    <div class="container_module">
        <div class="module container_video_50 scroll_anchor">
            <a name="<?=$helicopter_pages[0]->title?>" class="ajax-link" data-href="<?=$helicopter_pages[0]->url?>">
                <video muted loop poster="<?=$config->urls->assets?>files/videos/<?=$helicopter_pages[0]->video_index;?>.jpg" preload="metadata">
                    <source src="<?=$config->urls->assets?>files/videos/<?=$helicopter_pages[0]->video_index;?>.m4v" type="video/mp4">
                </video>
                
                <div class="video_title">
                    <span><?=orthotypographie($helicopter_pages[0]->title);?></span>
                </div>

                <div class="black_filter"></div>
            </a>
        </div>
        <div class="module container_video_50">
        <a name="<?=$helicopter_pages[1]->title?>" class="ajax-link" data-href="<?=$helicopter_pages[1]->url?>">
                <video muted loop poster="<?=$config->urls->assets?>files/videos/<?=$helicopter_pages[1]->video_index;?>.jpg" preload="metadata">
                    <source src="<?=$config->urls->assets?>files/videos/<?=$helicopter_pages[1]->video_index;?>.m4v" type="video/mp4">
                </video>
                
                <div class="video_title">
                    <span><?=orthotypographie($helicopter_pages[1]->title);?></span>
                </div>

                <div class="black_filter"></div>
            </a>
        </div>
    </div>

    <div class="container_video_100">
        <a name="<?=$helicopter_pages[2]->title?>" class="ajax-link" data-href="<?=$helicopter_pages[2]->url?>">
            <video muted loop poster="<?=$config->urls->assets?>files/videos/<?=$helicopter_pages[2]->video_index;?>.jpg" preload="metadata">
                <source src="<?=$config->urls->assets?>files/videos/<?=$helicopter_pages[2]->video_index;?>.m4v" type="video/mp4">
            </video>

            <div class="video_title">
                <span><?=orthotypographie($helicopter_pages[2]->title);?></span>
            </div>
            
            <div class="black_filter"></div>
        </a>
    </div>

    <footer class="container_module">
        <div class="module left padding">
            <div class="contact">
            <?=orthotypographie($page->body_sup);?> 
            </div>
        </div>

        <div class="module right padding">
            <ul class="credits">
                <li><small>Design&#8239;: <a data-href="http://www.intercouleur.fr/">intercouleur</a>&#8193;</small></li>
                <li><small>Son&#8239;: Antoine Boj&#8193;</small></li>
                <li><small>Code&#8239;: <a data-href="http://www.vincent-maillard.fr/">Vincent Maillard</a></small></li>
            </ul>
        </div>
    </footer>

</div>
