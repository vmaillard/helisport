
<div class="container_module news_page">
    
    <div class="module left padding">
        <div class="sticky">
            <?=orthotypographie($page->body);?> 
        </div>
<?php // on chope le champ contact de la home ?>
        <div class="contact">
            <?=orthotypographie($homepage->body_sup);?> 
        </div>
     </div>

    <div class="module right padding">
<?php foreach ($page_children as $child): ?>

    <article>
        <div class="container_h4">
            <h4><?=orthotypographie($child->title);?></h4>
            <span><?=$child->date?></span>
        </div>
        <?=orthotypographie($child->body);?> 
<?php /* $gallery_image est créée en Hanna Code */ ?>
<?php /* tableau, fichier en Hanna Code */ ?>
    
    </article>


<?php endforeach ?>

    </div> <!--/module-->

    
    <footer class="padding hidden_desktop">
        <?=orthotypographie($homepage->body_sup);?> 
    </footer>
    
</div> <!--/container_module-->




