
    <div class="container_video_100 button_scroll">
        <video muted loop poster="<?=$config->urls->assets?>files/videos/<?=$page->video_index;?>.jpg" preload="metadata">
            <source src="<?=$config->urls->assets?>files/videos/<?=$page->video_index;?>.m4v" type="video/mp4">
        </video>
    </div>

    <div class="container_module scroll_leading scroll_anchor">
        <div class="module left padding">
            <div class="sticky">
                <h3><?=orthotypographie($page->title);?></h3>
                <?=orthotypographie($page->body);?> 
            </div>
<?php // on chope le champ contact de la home ?>
            <div class="contact">
                <?=orthotypographie($homepage->body_sup);?> 
            </div>
        </div>

        <div class="module right padding">
<?php foreach ($page_children as $child): ?>

        <article>
            <h4 class="counter"><?=$child->title?></h4>
            <?=orthotypographie($child->body);?> 
            <?php // tableau, galerie et fichier en hanna code?>

        </article>
<?php endforeach // end foreach à travers page_children as child ?>

        </div>

        <footer class="padding hidden_desktop">
            <?=orthotypographie($homepage->body_sup);?> 
        </footer>

    </div> <!--/container_module-->
