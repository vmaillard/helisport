<?php

if(!$config->ajax) include("inc/head.php");

if($page->template->name == 'home'):
	include("inc/body_home.php");
elseif ($page->template->name == 'helicopter-page'):
    include("inc/body_helicopter.php");
elseif ($page->template->name == 'gallery-page'):
    include("inc/body_gallery.php");
elseif ($page->template->name == 'news-page'):
    include("inc/body_news.php");
endif;

if(!$config->ajax) include("inc/foot.php");